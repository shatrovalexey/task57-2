#!/usr/bin/perl

use strict ;
use warnings ;
use threads ;
use Perl::Critic ;

use Pithub ;
use POSIX qw(strftime) ;
use Date::Parse qw(str2time) ;
use Time::HiRes qw(gettimeofday tv_interval) ;

# предположение, что в каждых сутках 24 часа и в каждой неделе 7 суток
use constant 'HOURS' => 0 .. 23 ;
use constant 'WDAYS' => 1 .. 7 ;
use constant 'ACCESS_TOKEN_FILE' => '.access_token' ;
use constant 'SINCE' => time( ) - 365 * 24 * 60 * 60 ;
use constant 'SINCE_FMT' => strftime( '%Y-%m-%dT%H:%M:%SZ' , localtime( &SINCE( ) ) ) ;
use constant 'PER_PAGE' => 1e3 ;

INIT {
	@::started = gettimeofday( ) ;

	warn( 'Started at: ' . localtime( ) ) ;
}

*DESTROY = $::SIG{ '__DIE__' } = $::SIG{ 'INT' } = $::SIG{ 'TERM' } = sub {
	warn( 'Finished at: ' . localtime( ) ) ;
	warn( 'Total time (sec): ' . tv_interval( \@::started , [ gettimeofday( ) ] ) ) ;

	exit( 0 ) ;
} ;

# пользователь из командной строки
my ( $user , @args ) = @ARGV or die( 'No user given' ) ;

close( STDERR ) unless grep {
	$_ eq '--debug' ;
} @args ;

# получение OAuth access_token

my ( $access_token_rx , $access_token ) = qr{\S+} ;

until( undef( ) ) {
	next( ) unless open( my $fh , '<' , &ACCESS_TOKEN_FILE( ) ) ;
	next( ) unless <$fh> =~ $access_token_rx ;

	$access_token = $& ;

	last( ) ;
} continue {
	print( 'Enter OAuth access_token: ' ) ;

	next( ) unless <STDIN> =~ $access_token_rx ;
	die( 'Internal error' ) unless open( my $fh , '>' , &ACCESS_TOKEN_FILE( ) ) ;

	$access_token = $& ;

	$fh->print( $access_token ) ;

	last( ) ;
}

# репозитории пользователя
my $phh = Pithub->new(
	'token' => $access_token ,
) ;
$phh->auto_pagination( 1 ) ;

my $repos = $phh->repos( );
my $repos_list = $repos->list(
	'user' => $user ,
	'query' => {
		'per_page' => &PER_PAGE( ) ,
	} ,
) ;

die( 'No repos' ) unless $repos_list->success( ) ;

# результат

my %result = map {
	$_ => {
		map {
			$_ => 0 ;
		} &HOURS( )
	} ;
} &WDAYS( ) ;

# сбор данных о коммитах репозиториев

while ( my $repo = $repos_list->next( ) ) {
	next( ) unless str2time( $repo->{ 'pushed_at' } ) > &SINCE( ) ;

	my $commits_list = $repos->commits( )->list(
		'user' => $user ,
		'repo' => $repo->{ 'name' } ,
		'query' => {
			'since' => &SINCE_FMT( ) ,
		} ,
	) ;

	unless ( $commits_list->success( ) ) {
		warn( $commits_list->{ 'response' }{ '_content' } ) ;

		next( ) ;
	}

	threads->create( {
		'context' => 'list' ,
	} , sub {
		warn( 'Repo: ' . $repo->{ 'name' } ) ;

		while ( my $commit = $commits_list->next( ) ) {
			my $date = delete( $commit->{ 'commit' }{ 'committer' }{ 'date' } ) ;
			my ( $wday , $hour ) = ( localtime( str2time( $date ) ) )[ 6 , 2 ] ;

			warn( 'Commited at ' . threads->tid( ) . ': ' . $date ) ;

			$result{ ++ $wday }{ $hour } ++ ;
		}


		print( '-' x 10 , $repo->{ 'name' } , '-' x 10 , "\n" ) ;
		printf( '%4s' x ( 2 + &HOURS( ) ) , '' , &HOURS( ) , "\n" ) ;
		printf( '%4s' x ( 2 + &HOURS( ) ) , $_ , @{ $result{ $_ } }{ &HOURS( ) } , "\n" )
			foreach &WDAYS( ) ;
	} ) ;
}

$_->join( ) foreach threads->list( ) ;

__PACKAGE__->DESTROY( ) ;