#!/usr/bin/perl

use strict ;
use warnings ;
use Perl::Critic ;

use Pithub ;
use POSIX qw(strftime) ;
use Date::Parse qw(str2time) ;
use Time::HiRes qw(gettimeofday tv_interval) ;

# предположение, что в каждых сутках 24 часа и в каждой неделе 7 суток
use constant 'HOURS' => 0 .. 23 ;
use constant 'WDAYS' => 1 .. 7 ;
use constant 'ACCESS_TOKEN_FILE' => '.access_token' ;
use constant 'SINCE' => time( ) - 365 * 24 * 60 * 60 ;
use constant 'SINCE_FMT' => strftime( '%Y-%m-%dT%H:%M:%SZ' , localtime( &SINCE( ) ) ) ;
use constant 'PER_PAGE' => 1e3 ;

INIT {
	@::started = gettimeofday( ) ;

	warn( 'Started at: ' . localtime( ) ) ;
}
$::SIG{ '__DIE__' } = $::SIG{ 'INT' } = $::SIG{ 'TERM' } = sub {
	warn( 'Finished at: ' . localtime( ) ) ;
	warn( 'Total time (sec): ' . tv_interval( \@::started , [ gettimeofday( ) ] ) ) ;

	exit( 0 ) ;
} ;

# пользователь из командной строки
my ( $user ) = @ARGV or die( 'No user given' ) ;

my $access_token = sub {
	return +( ) unless open( my $fh , '<:encoding(utf-8)' , &ACCESS_TOKEN_FILE( ) ) ;
	return +( ) unless <$fh> =~ m{\S+}os ;

	$fh->close( ) ;

	$& ;
}->( ) or die( << "." ) ;
Create ${\&ACCESS_TOKEN_FILE( )} file and place there your OAuth access token to use the script.
.

# репозитории пользователя
my $phh = Pithub->new(
	'token' => $access_token ,
) ;
$phh->auto_pagination( 1 ) ;

my $repos = $phh->repos( );
my $repos_list = $repos->list(
	'user' => $user ,
	'query' => {
		'per_page' => &PER_PAGE( ) ,
	} ,
) ;

# результат
my %result = map {
	$_ => {
		map {
			$_ => 0 ;
		} &HOURS( )
	} ;
} &WDAYS( ) ;

# сбор данных о коммитах репозиториев

warn( $repos_list->{ 'response' }{ '_content' } )
	unless $repos_list->success( ) ;

while ( my $repo = $repos_list->next( ) ) {
	next( ) unless str2time( $repo->{ 'pushed_at' } ) > &SINCE( ) ;

	my $commits_list = $repos->commits( )->list(
		'user' => $user ,
		'repo' => $repo->{ 'name' } ,
		'query' => {
			'since' => &SINCE_FMT( ) ,
		} ,
	) ;

	unless ( $commits_list->success( ) ) {
		warn( $commits_list->{ 'response' }{ '_content' } ) ;

		next( ) ;
	}

	warn( 'Repo: ' . $repo->{ 'name' } ) ;

	while ( my $commit = $commits_list->next( ) ) {
		my $date = delete( $commit->{ 'commit' }{ 'committer' }{ 'date' } ) ;
		my ( $wday , $hour ) = ( localtime( str2time( $date ) ) )[ 6 , 2 ] ;

		warn( "Commited at: $date" ) ;

		$result{ ++ $wday }{ $hour } ++ ;
	}
}

# Собственно, вывод:
printf( '%4s' x ( 2 + &HOURS( ) ) , '' , &HOURS( ) , "\n" ) ;
printf( '%4s' x ( 2 + &HOURS( ) ) , $_ , @{ $result{ $_ } }{ &HOURS( ) } , "\n" ) foreach &WDAYS( ) ;