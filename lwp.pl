#!/usr/bin/perl

use strict ;
use warnings ;
use Perl::Critic ;

use JSON ;
use URI ;
use LWP::UserAgent ;
use POSIX qw(strftime) ;
use Date::Parse qw(str2time) ;

use constant 'HOURS' => 0 .. 23 ;
use constant 'WDAYS' => 1 .. 7 ;
use constant 'ACCESS_TOKEN_FILE' => '.access_token' ;
use constant 'SINCE' => time( ) - 365 * 24 * 60 * 60 ;
use constant 'SINCE_FMT' => strftime( '%Y-%m-%dT%H:%M:%SZ' , localtime( &SINCE( ) ) ) ;
use constant 'PER_PAGE' => 1e3 ;

# пользователь из командной строки
my ( $user ) = @ARGV or die( 'No user given' ) ;
my $access_token = sub {
	return +( ) unless open( my $fh , '<:encoding(utf-8)' , &ACCESS_TOKEN_FILE( ) ) ;
	return +( ) unless <$fh> =~ m{\S+}os ;

	$fh->close( ) ;

	$& ;
}->( ) or die( << "." ) ;
Create ${\&ACCESS_TOKEN_FILE( )} file and place there your OAuth access token to use the script.
.

my $json = JSON->new( )->utf8( 0 ) ;
my $lwp = LWP::UserAgent->new( ) ;
my $uri = URI->new( 'https://api.github.com/' ) ;

# результат
my %result = map {
	$_ => {
		map {
			$_ => 0 ;
		} &HOURS( )
	} ;
} &WDAYS( ) ;

# просмотр репозиториев пользователя

my $repo_page = 1 ;

until( undef( ) ) {
	$uri->path( '/users/' . $user . '/repos' ) ;

	my $resp = $lwp->get( $uri ,
		'access_token' => $access_token ,
		'per_page' => &PER_PAGE( ) ,
		'page' => $repo_page ++ ,
	) ;

	warn( $resp->content( ) ) ;

	last( ) unless $resp->is_success( ) ;

	my $repos = eval {
		$json->decode( $resp->content( ) ) ;
	} or last( ) ;

	while ( my $repo = shift( @$repos ) ) {
		warn( $repo->{ 'name' } ) ;

		# просмотр коммитов пользователя

		$uri->path( '/repos/' . $user . '/' . $repo->{ 'name' } . '/commits' ) ;

		until ( undef( ) ) {
			my $resp = $lwp->get( $uri ,
				'access_token' => $access_token ,
			) ;

			last( ) unless $resp->is_success( ) ;

			my $commits = eval {
				$json->decode( $resp->content( ) ) ;
			} or last( ) ;

			warn( scalar( @$commits ) ) ;

			while ( my $commit = shift( @$commits ) ) {
				my $date = delete( $commit->{ 'commit' }{ 'committer' }{ 'date' } ) ;
				my ( $wday , $hour ) = ( localtime( str2time( $date ) ) )[ 6 , 2 ] ;

				warn( "Commited at: $date" ) ;

				$result{ ++ $wday }{ $hour } ++ ;
			}

			last( ) ;
		}
	}
}

# Собственно, вывод:
printf( '%4s' x ( 2 + &HOURS( ) ) , '' , &HOURS( ) , "\n" ) ;
printf( '%4s' x ( 2 + &HOURS( ) ) , $_ , @{ $result{ $_ } }{ &HOURS( ) } , "\n" ) foreach &WDAYS( ) ;